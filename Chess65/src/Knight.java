public class Knight extends ChessPiece {
	
	/**
     * 
     * @param file
     * @param rank
     * @param name
     * Instance of a ChessPiece.
     */
    
    Knight(int file, int rank, String team){
        super(file,rank, team);
        super.type = "Knight";
        //setPossibleNextMoves(super.getCurrentPosition());
    }
    
    /**
     * @param Integer[]
     * This method sets all possible paths for this piece
     */

    void setPossibleNextMoves(Integer[] currentPosition){
        possibleNextMoves.clear();
        possibleNextMoves.add(Integer.toString(currentPosition[0]+1) + Integer.toString(currentPosition[1]+2));
        possibleNextMoves.add(Integer.toString(currentPosition[0]-1) + Integer.toString(currentPosition[1]+2));

        possibleNextMoves.add(Integer.toString(currentPosition[0]+2) + Integer.toString(currentPosition[1]+1));
        possibleNextMoves.add(Integer.toString(currentPosition[0]-2) + Integer.toString(currentPosition[1]+1));

        possibleNextMoves.add(Integer.toString(currentPosition[0]+2) + Integer.toString(currentPosition[1]-1));
        possibleNextMoves.add(Integer.toString(currentPosition[0]-2) + Integer.toString(currentPosition[1]-1));

        possibleNextMoves.add(Integer.toString(currentPosition[0]+1) + Integer.toString(currentPosition[1]-2));
        possibleNextMoves.add(Integer.toString(currentPosition[0]-1) + Integer.toString(currentPosition[1]-2));

        collisions();
    }

    /**
     * 
     * @param eatMoveRight
     * @param eatMoveLeft
     * @param currentPosition
     * This method modifies possible movements by considering any collosions with any piece.
     */
    
    public void collisions() {
        /**
         * traverse through all pieces to check for collisions
         */
        for(ChessPiece pieces : ChessGame.allPieces){
            // turn pieces current position to string
            Integer[] currPosInt = pieces.getCurrentPosition();
            String currPos = Integer.toString(currPosInt[0])+Integer.toString(currPosInt[1]);

            if(possibleNextMoves.contains(currPos) && pieces.team.equalsIgnoreCase(this.team)){
                possibleNextMoves.remove(currPos);
            }
        }
    }
}
