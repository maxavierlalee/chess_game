public class Pawn extends ChessPiece {
	String type = "Pawn";

	/**
     * 
     * @param file
     * @param rank
     * @param name
     * Instance of a ChessPiece.
     */
    
    public Pawn(int file, int rank, String name){
        super(file,rank, name);
        super.type = "Pawn";
    }

    /**
     * 
     * @return boolean
     * Checks if pawn has moved yet.
     */
    public boolean isFirstMove() {
        return isFirstMove;
    }
    
    /**
     * @param Integer[]
     * This method sets all possible paths for this piece
     */
    void setPossibleNextMoves(Integer[] currentPosition){
    	//System.out.println("Current position: "+Integer.toString(currentPosition[0])+Integer.toString(currentPosition[1]));
        possibleNextMoves.clear();
    	int deductorOrAdder;
    	if(team.equalsIgnoreCase("black")){
    	    deductorOrAdder = -1;
        } else{
    	    deductorOrAdder = 1;
        }

    	if((getCurrentPosition()[1] == 2 && team.equals("white"))|| (getCurrentPosition()[1] == 7) && team.equals("black")){
    	    isFirstMove = true;
        }
        if((getCurrentPosition()[1] == 7 && team.equals("white"))|| (getCurrentPosition()[1] == 2) && team.equals("black")){
            System.out.println("Promotion is set to true!");
            readyForPromotion = true;
        }
        if(isFirstMove){
            for(int i  = 1; i<3; i++){
                int newFile = currentPosition[0];
                int newRank = currentPosition[1] + deductorOrAdder*i;
                possibleNextMoves.add(Integer.toString(newFile) + Integer.toString(newRank));
            }
            isFirstMove = false;
        } else {
        	int newRank = currentPosition[1] + deductorOrAdder;
        	possibleNextMoves.add(Integer.toString(currentPosition[0]) + Integer.toString(newRank));
        }
        Integer[] eatMoveRight = {currentPosition[0] + 1, currentPosition[1] + 1 * deductorOrAdder};
        Integer[] eatMoveLeft = {currentPosition[0] - 1, currentPosition[1] + 1 * deductorOrAdder};
        
        collisions(eatMoveRight, eatMoveLeft, currentPosition);
    }
    
    /**
     * 
     * @param eatMoveRight
     * @param eatMoveLeft
     * @param currentPosition
     * This method modifies possible movements by considering any collosions with any piece.
     */
    void collisions(Integer[] eatMoveRight, Integer[] eatMoveLeft, Integer[] currentPosition) {
        /**
         * traverse through all pieces to check for collisions
         */
    	for(ChessPiece pieces : ChessGame.allPieces){
    	    String thisPos = this.getCurrentPosition()[0].toString() + this.getCurrentPosition()[1].toString();
            String currPos = pieces.getCurrentPosition()[0].toString()+pieces.getCurrentPosition()[1].toString();
            //System.out.println(currPos);

            if(pieces.getCurrentPosition()[0] == currentPosition[0] && (pieces.getCurrentPosition()[1] == currentPosition[1] + 1 || pieces.getCurrentPosition()[1] == currentPosition[1] - 1)){
                possibleNextMoves.remove(currPos);
                pieces.possibleNextMoves.remove(thisPos);
               // System.out.println("Removing: "+currPos + " ... for: "+this.toString());
            }

            if (eatMoveRight[0] == pieces.getCurrentPosition()[0] && eatMoveRight[1] == pieces.getCurrentPosition()[1] && !pieces.team.equals(this.team)) {
            	//System.out.println("\t\tI can eat!");
            	possibleNextMoves.add(Integer.toString(eatMoveRight[0]) + Integer.toString(eatMoveRight[1]));
            }
            if (eatMoveLeft[0] == pieces.getCurrentPosition()[0] && eatMoveLeft[1] == pieces.getCurrentPosition()[1] && !pieces.team.equals(this.team)) {
            	possibleNextMoves.add(Integer.toString(eatMoveLeft[0]) + Integer.toString(eatMoveLeft[1]));
            }
        }
    }
}
