import java.util.ArrayList;

public class Bishop extends ChessPiece {
//    public boolean attemptMove(int file, int rank){
//        /* TODO:
//            - override this method per subclass of ChessPiece to see if coordinate is possible with a piece's possible path
//            - check if during path it runs into any other pieces
//        */
//        return true;
//    }

    Bishop(int file, int rank, String team){
        super(file,rank, team);
        super.type = "Bishop";
        //setPossibleNextMoves(super.getCurrentPosition());
    }
    
    void setPossibleNextMoves(Integer[] currentPosition){
        possibleNextMoves.clear();
    	collisions(1, 1, currentPosition);
    	collisions(-1, 1, currentPosition);
    	collisions(1, -1, currentPosition);
    	collisions(-1, -1, currentPosition);
    }
    
    public void collisions(int multiplierFile, int multiplierRank, Integer[] currentPosition) {
        boolean pathIsBlocked = false;
        //int max = 8 - currentPosition[0];
        /**
         * traverse through size 8 to check collisions
         */
        for (int i = 1; i <= 8; i++) {
            int pathFile = currentPosition[0] + i*multiplierFile;
            int pathRank = currentPosition[1] + i*multiplierRank;
            for (int j = 0; j < ChessGame.allPieces.size(); j++) {
                Integer[] currPos = ChessGame.allPieces.get(j).getCurrentPosition();
                if (currPos[0] == pathFile && currPos[1] == pathRank && ChessGame.allPieces.get(j).team.equals(this.team)) {
                    pathIsBlocked = true;
                    break;
                } else if (currPos[0] == pathFile && currPos[1] == pathRank && !ChessGame.allPieces.get(j).team.equals(this.team)) {
                    possibleNextMoves.add(Integer.toString(pathFile) + Integer.toString(pathRank));
                    pathIsBlocked = true;
                    break;
                }
            }
            if (pathIsBlocked)
                break;
            possibleNextMoves.add(Integer.toString(pathFile) + Integer.toString(pathRank));
        }
    }
}
