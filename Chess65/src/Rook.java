public class Rook extends ChessPiece {

	/**
     * 
     * @param file
     * @param rank
     * @param name
     * Instance of a ChessPiece.
     */

    Rook(int file, int rank, String team){
        super(file,rank, team);
        super.type = "Rook";
        //setPossibleNextMoves(super.getCurrentPosition());
    }

    boolean isFirstMove = true;
    public boolean isFirstMove() {
        return isFirstMove;
    }
    
    /**
     * @param Integer[]
     * This method sets all possible paths for this piece
     */

    void setPossibleNextMoves(Integer[] currentPosition){
        possibleNextMoves.clear();
    	collisions(1, 0, currentPosition);
    	collisions(-1, 0, currentPosition);
    	collisions(0, 1, currentPosition);
    	collisions(0, -1, currentPosition);
    }
    
    /**
     * 
     * @param eatMoveRight
     * @param eatMoveLeft
     * @param currentPosition
     * This method modifies possible movements by considering any collosions with any piece.
     */
    
    public void collisions(int multiplierFile, int multiplierRank, Integer[] currentPosition) {
        boolean pathIsBlocked = false;
        //int max = 8 - currentPosition[0];
        /**
         * traverse size 8 to check for collisions
         */
        for (int i = 1; i <= 8; i++) {
            int pathFile = currentPosition[0] + i*multiplierFile;
            int pathRank = currentPosition[1] + i*multiplierRank;
            for (int j = 0; j < ChessGame.allPieces.size(); j++) {
                Integer[] currPos = ChessGame.allPieces.get(j).getCurrentPosition();
                if (currPos[0] == pathFile && currPos[1] == pathRank && ChessGame.allPieces.get(j).team.equals(this.team)) {
                    pathIsBlocked = true;
                    break;
                } else if (currPos[0] == pathFile && currPos[1] == pathRank && !ChessGame.allPieces.get(j).team.equals(this.team)) {
                    possibleNextMoves.add(Integer.toString(pathFile) + Integer.toString(pathRank));
                    pathIsBlocked = true;
                    break;
                }
            }
            if (pathIsBlocked)
                break;
            possibleNextMoves.add(Integer.toString(pathFile) + Integer.toString(pathRank));
        }
    }
}
