/**
 * @author Matthew Lee
 * @author Xavier La Rosa
 */

public class Main {

    public static void main(String[] args) {
        /**
         * All you need to do to start a chess game
         */
        ChessGame game = new ChessGame();
        game.start();
    }
}
