/**
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class ChessPiece {

	private Integer[] currentPosition = {0,0};
    private String name;
    boolean isPlaced;
    String type;
    protected String team;
    boolean isFirstMove = true;
    boolean readyForPromotion = false;
    
    /**
     * 
     * @param file
     * @param rank
     * @param name
     * Instance of a ChessPiece.
     */
    
    ChessPiece(int file, int rank, String name){ // each chess piece must start with a given file and rank and label
        this.currentPosition[0] = file;
        this.currentPosition[1] = rank;
        this.name = name;

        char[] nameArray = name.toCharArray();
        if(nameArray[0] == 'b'){
            this.team = "black";
        } else{
            this.team = "white";
        }
    }
    
    /**
     * 
     * @param file
     * @param rank
     * This method refreshes all possible moves of a current piece based on their position, as well as sets their current position
     * 
     */

    public void setCurrentPosition(int file, int rank){
        for (int i = 0; i < ChessGame.allPieces.size(); i++) {
            if(!ChessGame.allPieces.get(i).type.equals("Pawn") && !ChessGame.allPieces.get(i).isFirstMove){
                ChessGame.allPieces.get(i).possibleNextMoves.clear();
            }
        }
        for (int i = 0; i < ChessGame.whitePieces.size(); i++) {
            if(!ChessGame.whitePieces.get(i).type.equals("Pawn") && !ChessGame.allPieces.get(i).isFirstMove){
                ChessGame.whitePieces.get(i).possibleNextMoves.clear();
            }
        }
        for (int i = 0; i < ChessGame.blackPieces.size(); i++) {
            if(!ChessGame.blackPieces.get(i).type.equals("Pawn") && !ChessGame.allPieces.get(i).isFirstMove){
                ChessGame.blackPieces.get(i).possibleNextMoves.clear();
            }
        }
        this.currentPosition[0] = file; // x position
        this.currentPosition[1] = rank; // y position
        //setPossibleNextMoves(currentPosition);
        for (int i = 0; i < ChessGame.allPieces.size(); i++) {
            ChessGame.allPieces.get(i).setPossibleNextMoves(ChessGame.allPieces.get(i).getCurrentPosition());
        }
        for (int i = 0; i < ChessGame.whitePieces.size(); i++) {
            ChessGame.whitePieces.get(i).setPossibleNextMoves(ChessGame.whitePieces.get(i).getCurrentPosition());
        }
        for (int i = 0; i < ChessGame.blackPieces.size(); i++) {
            ChessGame.blackPieces.get(i).setPossibleNextMoves(ChessGame.blackPieces.get(i).getCurrentPosition());
        }
    }
    
    /**
     * 
     * @return Integer[]
     * This method returns the piece's current position
     * 
     */
    Integer[] getCurrentPosition(){
        return this.currentPosition;
    }

    /**
     * 
     * @param name
     * This method sets the name of the respective piece.
     * 
     */
    public void setName(String name){
        this.name = name;
    }
    
    /**
     * 
     * @return String
     * This method returns the name of the respectiev piece.
     * 
     */
    String getName(){
        return this.name;
    }

    /**
     * 
     * @param currentPosition
     * THis method is meant to be overridden by subclasses to determine possible movements.
     * 
     */
    void setPossibleNextMoves(Integer[] currentPosition){
        /*
            This method should be overridden by each subclass
         */
    }

    List<String> possibleNextMoves = new ArrayList<>();
    
    /**
     * 
     * @param file
     * @param rank
     * @return boolean
     * This method ensures that an attempted move is possible based on their possible paths.
     * 
     */
    public boolean attemptMove(int file, int rank){ // this method holds true for bishop, knight, and queen
    
        Integer[] attemptedPosition = {file, rank};
        if(attemptedPosition[0] == currentPosition[0] && attemptedPosition[1] == currentPosition[1]){ // base case
            System.out.println("\t\tYou cant set the new position of a piece to its current position");
            return false;
        } else if(possibleNextMoves.contains(Integer.toString(file)+Integer.toString(rank))){ // list of possible moves contains the move wanted

            for(ChessPiece piece : ChessGame.allPieces){
                if(piece.getCurrentPosition()[0] == file && piece.getCurrentPosition()[1] == rank){
                    System.out.println("\t\tEating opponent piece: "+piece.toString());
                    ChessGame.allPieces.remove(piece);
                    break;
                }
            }

            System.out.println("\t\tThis move is possible!");
            System.out.println("\t\tPrevious position: "+"("+convertFileNumberToLetter(getCurrentPosition()[0])+", "+getCurrentPosition()[1]+")");
            int backupFile = getCurrentPosition()[0];
            int backupRank = getCurrentPosition()[1];

            setCurrentPosition(file, rank);

            if(team.equals("black")){
                ChessGame.isThereACheck(ChessGame.whitePieces);
            } else{
                ChessGame.isThereACheck(ChessGame.blackPieces);
            }
            if(ChessGame.bIsChecked || ChessGame.wIsChecked ){
                System.out.println("\t\tError, king is still in check");
                setCurrentPosition(backupFile, backupRank);
                return false;
            }
            System.out.println("\t\tCurrent position: "+"("+convertFileNumberToLetter(getCurrentPosition()[0])+", "+getCurrentPosition()[1]+")");
            return true;
        } else{
            System.out.println("\t\tThis move is not possible!");

            return false;
        }
    }
    
    /**
     * 
     * @param file
     * @param rank
     * @return booelean
     * This method is exactly attemptMove but used only within the CheckMate method.
     * 
     */
    
    public boolean attemptMove2(int file, int rank){ // this method holds true for bishop, knight, and queen

        int backupFile = getCurrentPosition()[0];
        int backupRank = getCurrentPosition()[1];
        Integer[] attemptedPosition = {file, rank};
        /**
         * Setup base case
         */
        if(attemptedPosition[0] == currentPosition[0] && attemptedPosition[1] == currentPosition[1]){
            setCurrentPosition(backupFile, backupRank);// base case
            return false;
        } else if(possibleNextMoves.contains(Integer.toString(file)+Integer.toString(rank))){ // list of possible moves contains the move wanted

            for(ChessPiece piece : ChessGame.allPieces){
                if(piece.getCurrentPosition()[0] == file && piece.getCurrentPosition()[1] == rank){
                    ChessGame.allPieces.remove(piece);
                    break;
                }
            }

            // This is the attempt
            setCurrentPosition(file, rank); // this should be updating all possible moves

            // if king is still threat then set current position back to first pos and return false "king is still in check"

            if(team.equals("black")){
                ChessGame.isThereACheckForCheckMate(ChessGame.whitePieces);
            } else{
                ChessGame.isThereACheckForCheckMate(ChessGame.blackPieces);
            }
            if(ChessGame.bIsChecked || ChessGame.wIsChecked ){
                setCurrentPosition(backupFile, backupRank);
                return false;
            }
            setCurrentPosition(backupFile, backupRank);
            return true;
        }
        /**
         * move is not possible otherwise
         */
        else{
            setCurrentPosition(backupFile, backupRank);
            return false;
        }
    }

    /**
     * 
     * @param file
     * @return String
     * This method converts file ranks to letters and numbers to produce appropriate commands.
     * 
     */

    public static String convertFileNumberToLetter(int file){
        switch(file){
            case 1:
                return "a";
            case 2:
                return "b";
            case 3:
                return "c";
            case 4:
                return "d";
            case 5:
                return "e";
            case 6:
                return "f";
            case 7:
                return "g";
            case 8:
                return "h";
            default:
                System.out.println("Error: input is not convertible to letter");
                return "0";
        }
    }
    
    /**
     * 
     * @param file
     * @return int
     * This method takes file letters from commands and converts them to file to compare with other positions.
     * 
     */
    
    public static int convertFileLetterToNumber(Character file){
        switch(file){
            case 'a':
                return 1;
            case 'b':
                return 2;
            case 'c':
                return 3;
            case 'd':
                return 4;
            case 'e':
                return 5;
            case 'f':
                return 6;
            case 'g':
                return 7;
            case 'h':
                return 8;
            default:
                System.out.println("Error: input is not convertible to letter");
                return 0;
        }
    }
    
    /**
     * Overrides toString method from object class to print out type and position of a piece.
     */
    public String toString(){
        return "["+this.type+": "+convertFileNumberToLetter(this.currentPosition[0])+", "+this.currentPosition[1].toString()+"]";
    }
}
