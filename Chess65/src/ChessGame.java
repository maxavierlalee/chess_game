/**
 * @author Matthew Lee
 * @author Xavier La Rosa
 */

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * ChessGame class that handles all of the general rules within the game.
 */
class ChessGame {

	private boolean gameIsNotDone = true;
	private boolean inputIsNotValid = true;
	private String move = "";
	private String winner = "";
	private int boardWidth = 9;
	private Scanner strRead = new Scanner(System.in);
	static List<ChessPiece> whitePieces  = new ArrayList<>();
	static List<ChessPiece> blackPieces = new ArrayList<>();
	static List<ChessPiece> allPieces = new ArrayList<>();
	static boolean wIsChecked = false;
	static boolean bIsChecked = false;
	
	/**
	 * Initializes the game by creating the pieces, board and sets all pieces to there default places.
	 */
	public void start(){
		/**
		 * Creates the Pieces with their default places.
		 */
		createPieces();
		
		/**
		 * Draws the board respective to each piece's positions
		 */
		drawBoard();

		/**
		 * While there is no valid reason to end the game.
		 */
		while(gameIsNotDone){
			
			/**
			 * While the input is not in the correct format.
			 */
			
			while(inputIsNotValid){
				System.out.println("\nWhite team, please move");
				move = strRead.nextLine();
				if(verifyMove(move, "white")) {
					inputIsNotValid = false;
					if(isThereACheck(whitePieces)){
						if(isCheckMate("white")){
							System.out.println("Checkmate, game over");
							gameIsNotDone = false;
							winner="black";
						}
					}
				}
			}
			
			/**
			 * Input is now in the correct format.
			 */

			inputIsNotValid = true;
			System.out.println();

			/**
			 * While the input is not in the correct format.
			 */

			while(inputIsNotValid && gameIsNotDone){
				System.out.println("Black team, please move");
				move = strRead.nextLine();
				if(verifyMove(move, "black")){
					inputIsNotValid = false;
					if(isThereACheck(blackPieces)){
						if(isCheckMate("black")){
							System.out.println("Checkmate, game over");
							gameIsNotDone = false;
							winner="white";
						}
					}
				}
			}

			System.out.println();
			inputIsNotValid = true;
		}
		/**
		 * After the game is detected end we print winner if any
		 */
		end();
	}
	
	/**
	 * 
	 * @param oppPieces
	 * @return boolean
	 * This method is called after each move is done, checking if the King on each side is threatened by any other piece.
	 */
	static boolean isThereACheck(List<ChessPiece> oppPieces) {
		ChessPiece king;
		if(oppPieces.get(oppPieces.size()-1).team.equals("black")){
			king = whitePieces.get(whitePieces.size()-1);
		} else{
			king = blackPieces.get(blackPieces.size()-1);
		}
		int i = 0;
		int j = 0;
		for (ChessPiece p : oppPieces) {
			for (String moves : p.possibleNextMoves) {
				String locOfKing = king.getCurrentPosition()[0].toString() + king.getCurrentPosition()[1].toString();
				if(moves.equals(locOfKing)) {
					System.out.println("\t\tCheck was detected: "+p.toString() + ", with move: "+moves);
					if(king.team.equals("white")) {
						wIsChecked = true;
						return true;
					} else {
						bIsChecked = true;
						return true;
					}
					//return true;
				} else{
						wIsChecked = false;
						bIsChecked = false;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param oppPieces
	 * @return boolean
	 * This method is called after a move is predicted within a check instance to see if the King is still threatened after the respective predicted move.
	 * 
	 */
	static boolean isThereACheckForCheckMate(List<ChessPiece> oppPieces) {
		ChessPiece king;
		if(oppPieces.get(oppPieces.size()-1).team.equals("black")){
			king = whitePieces.get(whitePieces.size()-1);
		} else{
			king = blackPieces.get(blackPieces.size()-1);
		}
		System.out.println("Size of Pieces: "+oppPieces.size());
		for (int i = 0; i<oppPieces.size(); i++) {
			for (String moves : oppPieces.get(i).possibleNextMoves) {
				String locOfKing = king.getCurrentPosition()[0].toString() + king.getCurrentPosition()[1].toString();
				if(moves.equals(locOfKing)) {
					if(king.team.equals("white")) {
						wIsChecked = true;
						return true;
					} else {
						bIsChecked = true;
						return true;
					}
					//return true;
				} else{
					wIsChecked = false;
					bIsChecked = false;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param team
	 * @return boolean
	 * This method returns if there are no possible moves in which the threatened team can commit.
	 * 
	 */

	static boolean isCheckMate (String team) {
		List<String> allMovesBlack = new ArrayList<>();
		List<String> allMovesWhite = new ArrayList<>();
		for(ChessPiece p : blackPieces) {
			for(String move : p.possibleNextMoves){
				//System.out.println("\t\tPossible Move: "+x);
				allMovesBlack.add(move);
			}
		}
		for(ChessPiece p : whitePieces) {
			for(String move : p.possibleNextMoves){
				//System.out.println("\t\tPossible Move: "+x);
				allMovesWhite.add(move);
			}
		}
		if (team.equalsIgnoreCase("white")) {
			for (ChessPiece p : blackPieces) {
				for (String x : allMovesBlack) {
					if (!x.contains("-")) {
						int file = Integer.parseInt(Character.toString(x.charAt(0)));
						int rank = Integer.parseInt(Character.toString(x.charAt(1)));
						if(file <= 8 && rank <= 8) {
							//bIsChecked = true;
							//wIsChecked = true;
							if (p.attemptMove2(file, rank)) {
								System.out.println(p.toString() + file + " " + rank);
								return false;
							}
						}
					}
				}
			}
		} else {
			for (ChessPiece p : whitePieces) {
				for (String x : allMovesWhite) {
					if(!x.contains("-")){

						int file = Integer.parseInt(Character.toString(x.charAt(0)));
						int rank = Integer.parseInt(Character.toString(x.charAt(1)));
						if(file <= 8 && rank <= 8){
							//bIsChecked = true;
							//wIsChecked = true;
							if (p.attemptMove2(file, rank)) {
								System.out.println(p.toString() + file + " " + rank);
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * 	
	 * @param move
	 * @param team
	 * @return boolean
	 * This method ensures that a move is a possible move based on the respective piece.
	 *
	 */
	
	private boolean verifyMove(String move, String team){ // check if user input is acceptable
		//System.out.println("\t\tInside verifyMove function");
		String[] subStrings = move.toLowerCase().split(" ");
		//System.out.println("\t\tMove split into substrings: "+Arrays.toString(subStrings));

		if(move.equalsIgnoreCase("resign")){ // base case 1
			gameIsNotDone = false;
			System.out.println(team+" team has called a resign! Automatic winner to opponent!");
			switch(team){
				case "white":
					winner = "black";
					break;
				case "black":
					winner = "white";
			}
			return true;
		}

		if(subStrings.length > 4 || subStrings.length < 2){ // base case 2
			System.out.println("\t\tInput is not in correct format");
			return false;
		}

		if(!containsOneLetterOneNumber(subStrings[0]) || !containsOneLetterOneNumber(subStrings[1])){ // if coordinates are not in right format
			System.out.println("\t\tInput is not in correct format");
			return false;
		}

		if(move.toLowerCase().contains("draw?")){ // if draw is requested
			System.out.println("\t\tDraw was found");
			inputIsNotValid = true;
			while(inputIsNotValid){
				System.out.println("\nPlayer has requested to draw. Do you accept the draw? (y/n)");
				String answer = strRead.nextLine();
				switch(answer.toLowerCase()){
					case "y":
						System.out.println("Draw has been confirmed!");
						inputIsNotValid = false;
						gameIsNotDone = false;
						return true;
					case "n":
						System.out.println("Draw has been denied. Continuing with game.\n");
						inputIsNotValid = false;
						break;
					default:
						System.out.println("Input is not 'y' or 'n'. Was not able to confirm answer.");
				}
			}
			inputIsNotValid = true;
		}

		if(subStrings.length > 2 && !subStrings[2].equalsIgnoreCase("draw?")){ // if a promotion is requested
			// loop through team pieces to see if selected argument is in fact a pawn
			if(team.equals("black")){
				for(ChessPiece b : blackPieces){
					String bPieceLocation = ChessPiece.convertFileNumberToLetter(b.getCurrentPosition()[0]) + b.getCurrentPosition()[1];
					//System.out.println("Pawn to check if possible: "+bPieceLocation + ", and compare to: "+subStrings[0]);
					if(bPieceLocation.equals(subStrings[0]) ){
						//System.out.println("inside sdlfkjasldkfjs");
						int tempFile = b.getCurrentPosition()[0];
						int tempRank = b.getCurrentPosition()[1];
						int index = blackPieces.indexOf(b);

						switch(subStrings[2]){
							case "b":
								System.out.println("\t\tPromotion to Bishop");
								//blackPieces.set(index, new Bishop(tempFile, tempRank, "bB"));
								break;
							case "n":
								System.out.println("\t\tPromotion to Knight");
								//blackPieces.set(index, new Knight(tempFile, tempRank, "bN"));
								break;
							case "r":
								System.out.println("\t\tPromotion to Rook");
								//blackPieces.set(index, new Rook(tempFile, tempRank, "bR"));
								break;
							default:
								if(!subStrings[2].equals("q")){
									System.out.println("\t\tError with promotion");
									return false;
								} else{
									System.out.println("\t\tPromotion to Queen");
									//blackPieces.set(index, new Queen(tempFile, tempRank, "bQ"));
									break;
								}
						}
					}
				}
			} else{
				for(ChessPiece w : whitePieces){
					String wPieceLocation = ChessPiece.convertFileNumberToLetter(w.getCurrentPosition()[0]) + w.getCurrentPosition()[1];
					//System.out.println("Pawn to check if possible: "+wPieceLocation + ", and compare to: "+subStrings[0] + ", w promotion: "+w.readyForPromotion);
					if(wPieceLocation.equals(subStrings[0])){
						//System.out.println("inside sdlfkjasldkfjs");
						int tempFile = w.getCurrentPosition()[0];
						int tempRank = w.getCurrentPosition()[1];
						int index = whitePieces.indexOf(w);

						switch(subStrings[2]){
							case "b":
								System.out.println("\t\tPromotion to Bishop");
								//whitePieces.set(index, new Bishop(tempFile, tempRank, "wB"));
								break;
							case "n":
								System.out.println("\t\tPromotion to Knight");
								//whitePieces.set(index, new Knight(tempFile, tempRank, "wN"));
								break;
							case "r":
								System.out.println("\t\tPromotion to Rook");
								//whitePieces.set(index, new Rook(tempFile, tempRank, "wR"));
								break;
							default:
								if(!subStrings[2].equals("q")){
									System.out.println("\t\tError with promotion");
									return false;
								} else{
									System.out.println("\t\tPromotion to Queen");
									//whitePieces.set(index, new Queen(tempFile, tempRank, "wQ"));
									break;
								}
						}
					}
				}
			}

			return false;
		}


		if(containsOneLetterOneNumber(subStrings[0]) && containsOneLetterOneNumber(subStrings[1])){ // make a move if in correct simple format for arg 1 and 2
			System.out.println("\t\tInput is in correct format");
			if(team.equalsIgnoreCase("black") && findSelectedPiece(blackPieces, subStrings, team)){ // selected piece and successfully moved it
				return true;
			} else if(team.equalsIgnoreCase("white") && findSelectedPiece(whitePieces, subStrings, team)){ // selected piece and successfully moved it
				return true;
			} else if(team.equalsIgnoreCase("white") && findSelectedPiece(blackPieces, subStrings, team)){
				return false;
			} else if(team.equalsIgnoreCase("black") && findSelectedPiece(whitePieces, subStrings, team)){
				return false;
			}
		}
		System.out.println("\t\tIllegal move, try again");
		return false;
	}

	/**
	 * 
	 * @param currentPieces
	 * @param subStrings
	 * @param team
	 * @return boolean
	 * This method returns true if the first argument matches a piece within the respective team's list
	 * 
	 */
	
	private boolean findSelectedPiece(List<ChessPiece> currentPieces, String[] subStrings, String team){ // see if selected piece arg matches a piece on board
		for(int i = 0; i<currentPieces.size(); i++){
			Integer[] currPos = currentPieces.get(i).getCurrentPosition();

			String currFile = ChessPiece.convertFileNumberToLetter(currPos[0]);
			String currPosition = currFile + Integer.toString(currPos[1]);

			if(currPosition.equalsIgnoreCase(subStrings[0])){ // a piece has been matched
				System.out.println("\t\tChess Piece selected correctly!");

				char charFile = subStrings[1].charAt(0);
				int newFile = ChessPiece.convertFileLetterToNumber(charFile);
				int newRank = Integer.parseInt(subStrings[1].substring(1));
				if(team.equalsIgnoreCase(currentPieces.get(i).team) && currentPieces.get(i).attemptMove(newFile, newRank)){ // new move is made
					drawBoard();
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param coordinate
	 * @return boolean
	 * This method returns true if the first and second arguments combinations of letters and numbers to help the verifyMove() method.
	 * 
	 */

	private boolean containsOneLetterOneNumber(String coordinate){ // make sure arg 1 and 2 are in correct format
		if(coordinate.length()!= 2) {
			return false;
		}

		int rank;
		try {
			rank = Character.getNumericValue(coordinate.charAt(1));
			char file = Character.toLowerCase(coordinate.charAt(0));
			if((rank >= 1 && rank <= 8) && (file >= 'a' && file <= 'h')){
				//System.out.println("\t\tSuccess: coordinate is correct format");
				return true;
			}
			else{
				System.out.println("\t\tError: coordinate is not correct format.");
			}
		} catch (Exception e) {
			System.out.println("\t\tError: coordinate is not correct format.");
			return false;
		}
		return false;
	}
	
	/**
	 * This method prints the respective winner of the game and ends it.
	 */

	private void end(){ // print the outcome of the game
		//System.out.println("Inside end function");
		switch(winner){
			case "white":
				System.out.println("White team is the winner!");
				break;
			case "black":
				System.out.println("Black team is the winner!");
				break;
			default:
				System.out.println("There is no winner for this game.");
				break;
		}
	}

	
	/**
	 * This method updates the board based on initial pieces positions as well as newly placed ones.
	 */
	
	public void drawBoard() { // updates the board by traversing through locations per piece
		int rank;
		int file;
		String[] files = new String[] {" a ", " b ", " c ", " d ", " e ", " f ", " g ", " h "};
		System.out.println();

		for (rank = 8; rank > 0; rank--) {
			for (file = 0; file < 8; file++) {
				if (isBlack(file, rank) && !isOccupied(file + 1, rank)) {
					System.out.print("## ");
				}
				else if (isOccupied(file + 1, rank)) {

				}
				else if (!isBlack(file, rank) && !isOccupied(file+1, rank)){
					System.out.print("   ");
				}
				if (file == 7 && rank > 0) {
					System.out.print(rank);
				}
			}
			System.out.println();
			if (rank == 1) {
				for (String x: files) {
					System.out.print(x);
				}
			}
		}

		System.out.println();
		for (ChessPiece i : allPieces) {
			i.isPlaced = false;
		}
		for (int i = 0; i < allPieces.size(); i++) {
			allPieces.get(i).setPossibleNextMoves(allPieces.get(i).getCurrentPosition());
		}
    }

	/**
	 * 
	 * @param file
	 * @param rank
	 * @return boolean
	 * This checks if a tile is black.
	 */
	
	private static boolean isBlack (int file, int rank) { // check if tile is black
		if ((file + 1) % 2 == 0 && rank % 2 == 0) {
			return true;
		} else if ((file + 1) % 2 == 1 && rank % 2 == 1) {
			return true;
		} else{
			return false;
		}
	}
	
	/**
	 * 
	 * @param file
	 * @param rank
	 * @return boolean
	 * This checks whether or not a tile has a occupying piece in its file and rank position.
	 */

	private static boolean isOccupied (int file, int rank) { // check if spot is occupied by piece
		for (int i = 0; i < allPieces.size(); i++) {
			Integer[] wCurrPos = allPieces.get(i).getCurrentPosition();
			if (wCurrPos[0] == file && wCurrPos[1] == rank && allPieces.get(i).isPlaced == false) {
				System.out.print(allPieces.get(i).getName() + " ");
				allPieces.get(i).isPlaced = true;
				return true;
			}
		}
		return false;
	}

	/**
	 * This method creates all instances of each piece and places them in their defaul positions.
	 */
	void createPieces(){ // instantiates each piece with their origin location
		/**
		 * create pawn pieces for both teams
		 */
		for(int i = 0; i < 8; i++){ // for pawns
			blackPieces.add(new Pawn((i+1), 7, "bp"));
			whitePieces.add(new Pawn((i+1), 2, "wp"));
		}

		/**
		 * create rook and bishop pieces for both teams
		 */
		for(int i = 0; i<3; i++){ // for rooks, knights, and bishops
			switch(i+1){
				case 1:
					blackPieces.add(new Rook(i+1, 8, "bR"));
					whitePieces.add(new Rook(i+1, 1, "wR"));
					blackPieces.add(new Rook(boardWidth-(i+1), 8, "bR"));
					whitePieces.add(new Rook(boardWidth-(i+1), 1, "wR"));
					break;
				case 2:
					blackPieces.add(new Knight(i+1,8, "bN"));
					whitePieces.add(new Knight(i+1, 1, "wN"));
					blackPieces.add(new Knight(boardWidth-(i+1),8, "bN"));
					whitePieces.add(new Knight(boardWidth-(i+1),1, "wN"));
					break;
				case 3:
					blackPieces.add(new Bishop(i+1,8, "bB"));
					whitePieces.add(new Bishop(i+1, 1, "wB"));
					blackPieces.add(new Bishop(boardWidth-(i+1),8, "bB"));
					whitePieces.add(new Bishop(boardWidth-(i+1),1, "wB"));
					break;
			}
		}

		/**
		 * create Queen and King pieces for both teams
		 */
		for(int i = 3; i<5; i++){ // for kings, and queens
			switch(i+1){
				case 4:
					blackPieces.add(new Queen(i+1, 8, "bQ"));
					whitePieces.add(new Queen(i+1, 1, "wQ"));
					break;
				case 5:
					blackPieces.add(new King(i+1, 8, "bK"));
					whitePieces.add(new King(i+1, 1, "wK"));
					break;
			}
		}
		allPieces.addAll(whitePieces);
		allPieces.addAll(blackPieces);
		
		// printPieces(); // use this to test out if the pieces are in correct format
	}
}