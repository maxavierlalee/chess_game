# Chess65 #


*Matthew Lee & Xavier La Rosa, (Group #65)*

## Objective ##
* Create a 2-Player chess game implementing Object Oriented Programming design solutions.

## Tools ##
* Eclipse/IntelliJ IDEA
* BitBucket & Source Tree
* Java
* OOP

## Main Features ##
* There are black and white pieces.
* User can select a coordinate for a chess piece and select a coordinate for its destination.
* User can request to draw the game
* User can promote pawns when logical to the rules of Chess (default to Queen)
* Program displays "Check" when logical to the rules of Chess
* Reports winner when game terminates

## How To Run ##


## Notes ##
